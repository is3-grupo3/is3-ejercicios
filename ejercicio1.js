// Declarar un vector de 6 elementos con 3 tipos de datos
let vector = [1, 'dos', true, 4.5, 'cinco', false];

// a)Imprimir el vector en la consola
console.log(vector);

// b)Imprimir el primer y último elemento del vector usando sus índices
console.log(vector[0]);
console.log(vector[vector.length - 1]);

// c)Modificar el valor del tercer elemento
vector[2] = 'tres';

// d)Imprimir la longitud del vector
console.log('Longitud del vector:', vector.length);

// e)Agregar un elemento al vector usando "push"
vector.push(6);
console.log(vector);

// f)Eliminar el último elemento y imprimirlo usando "pop"
const elementoEliminado = vector.pop();
console.log('Elemento eliminado:', elementoEliminado);

// g)Agregar un elemento en la mitad del vector usando "splice"
vector.splice(3, 0, 'nuevoElemento');

// h)eliminar el primer elemento usando "shift"
const primerElemento = vector.shift();
console.log('Primer elemento eliminado:', primerElemento);

// i)gregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primerElemento);
